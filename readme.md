1. Name data types in JS?
   There are 8 types in JavaScrpit.
   1)String
   2)Number
   3)Boolean
   4)Object
   5)Bigint
   6)Symbol
   7)Null , but JS shows «object», if you use typeof null 8) Undefined
   Of wich 7 types are primitive and only object is non-primitive data types

2. How to check that data type is an object?
   typeof === «object»
3. How to check that data type is an array?
   Array.isArray()

4. Name all ways to create an array?
   1)const/let a = []
   2)Array.from()
   3)new Array()
   4)Array.of()
   5)String.split()
   6)Object.keys, Object.values, Object.entries

5. What is recursion?
   The act of a function calling itself, recursion is used to solve problems that contain smaller sub-problems. A recursive function can receive two terms: a base case (ends recursion) or a recursive case (resumes recursion).

6. How to check an object that it is empty?
   1)Object.keys(obj).length === 0
   2)JSON.stringify(obj) === «{}» 3) Also you cant use different libraries such as Lodash where are they methods to solve this task

7. What is the difference between arrow functions and normal functions?

   1. The arrow function cannot be called before its initialization
   2. Arrow functions don't have their own bindings to this, arguments, or super, and should not be used as methods.
   3. Different syntax

8. What is the keyword "this" in methods?
   «this» is a reference to the current object — the object whose method or constructor is being called.For example:
   cosnt a = {
   name: «Ivan»,
   showName(){
   console.log(this.name)
   }//this.name === «Ivan»

9. What is “use strict”?
   The purpose of "use strict" is to indicate that the code should be executed in "strict mode
   Why use scrict mode ?
   In strict mode:
   some errors can be found faster,
   more dangerous and not useful features of JavaScript are either prohibited or result in an error.
