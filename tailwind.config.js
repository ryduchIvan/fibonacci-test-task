module.exports = {
	content: ['./src/**/*.{html,js,vue}'],
	theme:{
		extend:{
			backgroundColor:{
				primaryGray: "#f6f6f6",
				primaryGreen: "#2cc913"
			},
			colors:{
				primaryGray: "#9ca3af",
			}
		}
	}
  }